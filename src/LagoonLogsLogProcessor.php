<?php

namespace Drupal\lagoon_logs;

use Monolog\LogRecord;

class LagoonLogsLogProcessor {

  protected $processData;

  public function __construct(array $processData) {
    $this->processData = $processData;
  }

  /**
   * @param  LogRecord $record
   *
   * @return LogRecord
   */
  public function __invoke(LogRecord $record) {
    foreach ($this->processData as $key => $value) {
      if (empty($record[$key])) {
        $record[$key] = $value;
      }
    }
    return $record;
  }
}
